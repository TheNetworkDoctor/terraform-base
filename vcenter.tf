#
# MAIN section voor vsphere resource (VM)
#

resource "vsphere_virtual_machine" "worker" {
  for_each = { for k, v in var.worker: k => v if v.target_node == null}
  name             = "${each.value.name}.${local.base.vm_domain}"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  folder           = "${var.vsphere_vm_folder}"

  num_cpus = each.value.vcpu_count
  memory   = each.value.memory
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  cpu_hot_add_enabled    = true
  cpu_hot_remove_enabled = true
  memory_hot_add_enabled = true

  #
  # DISK section voor het maken van de disk(s)
  #

  disk {
    unit_number      = 0
    label            = "disk0"
    size             = each.value.disk_size
//    size             = "${data.vsphere_virtual_machine.template.disks.0.size}"
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }


  #
  # NETWORK section voor het maken van de network(s)
  #

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = each.value.name
        domain    = local.base.vm_domain
      }

      network_interface {
        ipv4_address = each.value.ip
        ipv4_netmask = each.value.netmask
      }
      dns_server_list = var.dns_server_list
      ipv4_gateway = each.value.gw
    }
  }
  
#
# Post creation actions
#

  provisioner "remote-exec"  {
    inline = [
#        "sudo mkdir /home/${var.vm_username}/.ssh",
#        "sudo chmod 700 /home/${var.vm_username}/.ssh",
#        "sudo touch /home/${var.vm_username}/.ssh/authorized_keys",
#        "sudo chmod 600 /home/${var.vm_username}/.ssh/authorized_keys",
        "sudo echo ${var.ssh_pub_key} >> /home/${var.vm_username}/.ssh/authorized_keys",
    ]
    connection {
        type     = "ssh"
        user     = "${var.vm_username}"
#        password = "${var.vm_password}"
        private_key = "${file("./files/automation-private-key.pem")}"
        host     = each.value.ip
    }
  }
}