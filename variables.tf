variable "vsphere_user" {
  type = string
}
variable "vsphere_password" {
  type = string
}

variable "pm_token_id" {
  type = string
}
variable "pm_secret" {
  type = string
}

variable "netbox_api_key" {
  type = string
}

variable "powerdns_api_key" {
  type = string
}

variable "netbox_host" {
  type = string
}

variable "vmware_host" {
  type = string
}

variable "proxmox_host" {
  type = string
}

variable "powerdns_host" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "vm_domain_prefix" {
  type = string
}


variable "vsphere_cluster_id" {
  type = string
}
variable "proxmox_cluster_id" {
  type = string
}
variable "netbox_vm_vlan_name" {
  type = string
}
variable "netbox_site_id_name" {
  type = string
}

variable "vsphere_datacenter" { 
  type = string
}
variable "vsphere_datastore" { 
  type = string
}
variable "vsphere_compute_cluster" { 
  type = string
}
variable "vsphere_network" { 
  type = string
}
variable "vsphere_vm_template" { 
  type = string
}
variable "vsphere_vm_folder" { 
  type = string
}
variable "dns_server_list"{
    default = ["1.1.1.1"]
    description = "What DNS servers do you want?"
}


variable "proxmox_vm_template" { 
  type = string
}
variable "proxmox_vm_network" { 
  type = number
}
variable "proxmox_datastore" { 
  type = string
}


variable "vm_username" {
  type = string
}
variable "ssh_pub_key" {
  type = string
}


variable "worker" {
  description = "Hier komen de settings voor de VM's"
  type = list(object({
    name  = string
    ip = string
    gw = string
    netmask = number
    vcpu_count = number
    memory = number
    disk_size = number
    target_node = string
    nb_role_id = number
    nb_tag_list = list(string)
  }))

  default = [
    {
      name = "default-1"
      ip = "0.0.0.0"
      gw = "0.0.0.0"
      netmask = "24"
      vcpu_count = "2"
      memory = "4096"
      disk_size = "50"
      target_node = null
      nb_role_id = null
      nb_tag_list  = ["managed-by-terraform"]
    },
    {
      name                          = "default-2"
      ip                            = "0.0.0.0"
      gw                            = "0.0.0.0"
      netmask                       = "24"
      vcpu_count                    = "2"
      memory                        = "4096"
      disk_size                     = "50"
      target_node                   = null
      nb_role_id                    = null
      nb_tag_list                   = ["managed-by-terraform"]
    }
  ]
}

