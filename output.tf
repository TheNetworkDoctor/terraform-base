/*

output "DC_ID" {
  description = "id of vSphere Datacenter"
  value       = data.vsphere_datacenter.dc.id
}

output "vSphere_var" {
  description = "vSphere VAR"
  value       = var.vsphere_datastore
}

output "vSphere_networking_vlan" {
  description = "vSphere networking vlan"
  value       = data.vsphere_network.network.id
}

output "netbox_vc_dc_id" {
  description = "netbox VC DC ID"
  value       = data.netbox_cluster.vm_cluster_01.id
}
output "netbox_pm_dc_id" {
  description = "netbox PM DC ID"
  value       = data.netbox_cluster.pm_cluster_01.id
}

output "netbox_vc_dc_site_id" {
  description = "netbox VC DC Site ID"
  value       = data.netbox_cluster.vm_cluster_01.site_id
}
output "netbox_pm_dc_site_id" {
  description = "netbox PM DC Site ID"
  value       = data.netbox_cluster.pm_cluster_01.site_id
}

output "netbox_vm_vlan_name_id" {
  value = data.netbox_vlan.search_by_name.id
}

output "netbox_vcenter_cluster_id" {
  value = data.netbox_cluster.vm_cluster_01.id
}

output "netbox_proxmox_cluster_id" {
  value = data.netbox_cluster.pm_cluster_01.id
}

output "local_base" {
  value = local.base
}

output "local_netbox" {
  value = local.netbox
}

output "worker" {
  value = var.worker
}

*/