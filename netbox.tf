# create device
resource "netbox_virtual_machine" "vm" {
  for_each = {for i, v in var.worker:  i => v}
    cluster_id   = each.value.target_node == null ? data.netbox_cluster.vm_cluster_01.id : data.netbox_cluster.pm_cluster_01.id
    name         = each.value.name
    site_id      = each.value.target_node == null ? data.netbox_cluster.vm_cluster_01.site_id : data.netbox_cluster.pm_cluster_01.site_id
    disk_size_gb = each.value.disk_size
    memory_mb    = each.value.memory
    vcpus        = each.value.vcpu_count
//    role_id      = "${each.value.nb_role_id}"
    tags         = each.value.nb_tag_list
}

# create IPs
resource "netbox_ip_address" "nb_ip" {
  for_each = {for i, v in var.worker:  i => v}
    ip_address = "${each.value.ip}/${each.value.netmask}"
    status     = "active"
    interface_id = netbox_interface.vm_e0[each.key].id
    dns_name   = "${each.value.name}"
}

# create interfaces
resource "netbox_interface" "vm_e0" {
  for_each = {for i, v in var.worker:  i => v}
    name               = each.value.target_node == null ? "ens32" : "eth0"
    enabled            = true
    mode               = "access"
    untagged_vlan      = data.netbox_vlan.search_by_name.id
    virtual_machine_id = netbox_virtual_machine.vm[each.key].id
}

#assign an IP of an interface as primary IP
resource "netbox_primary_ip" "vc_vm_primaryip" {
  for_each = {for i, v in var.worker:  i => v}
    virtual_machine_id = netbox_virtual_machine.vm[each.key].id
    ip_address_id      = netbox_ip_address.nb_ip[each.key].id
}