
# Terraform Deploy En Register playbook (DERP)
Terraform collection of templates to deploy VM's to multiple platforms at once.
Designed to run with VMware Vcenter and Proxmox.
Registration is done in Netbox&PowerDNS
Made for personal development enviroment, experiences may vary.
\*Nederlands en engels wordt door elkaar gebruikt, deal with it [puts on sunglasses]

## WIP

### TL;DR
Clone de repo, pas de `terraform.tfvars` file aan met "secrets" en "variables".
run: `terraform init` wacht 1 min, run: `terraform apply` (ga koffie halen) resources worden aangemaakt, `terraform destroy` om je "rotzooi" weer op te ruimen.
 
## Terraform
Terraform is gemaakt om zaken aan te maken "terraform apply", maar kan net zo gemakkelijk zaken verwijderen "terraform destroy".
\*dit wil niet zeggen dat je het niet kan misbruiken om permanente infra te deployen.
\*alle "*.tf" files worden geladen/uitgevoerd in paralel(volgorde is niet ingebouwd)

## Vcenter
Maakt gebruik van een Vcenter template waar een `clone` van gemaakt wordt

## Proxmox
Maakt gebruik van een proxmox `cloud-init` image om een vm te maken.

## Netbox
Maakt VM naam en IP config aan in Netbox en koppelt bepaalde `tags` aan de VM's

## PowerDNS
Maakt een A record voor iedere host aan.

## Folder structure (and the lack of)
* main.tf - Providers config, login/token data, "modules??", basis data (en wat legacy..)
* locals.tf - wordt gebruikt om herhaling in variablen en code te voorkomen (helaas nog niet helemaal gelukt)
* output.tf - optioneel, wordt gebruikt om "debug" variablen/code terug te geven tijdens "exec"
* variables.tf - defineert de variablen die gebruit worden, hoe deze gestructureerd moeten zijn, wat er ingevult moet worden en eventueel "default" gevulde variablen.
* terraform.tfvars - Example file gevult met `variablen` om aan te passen aan de omgeving.
* netbox.tf - Netbox specifieke resources/acties
* vcenter.tf - Vmware specifieke resources/acties
* powerdns.tf - Powerdns specifieke resources/acties
* proxmox.tf - Proxmox specifieke resources/acties

## Configuration / Variables
Configuratie wordt gedaan via variablen, een `terraform.tfvars.example` file is beschikbaar.

\* hernoem deze naar `terraform.tfvars` en hij wordt automatisch geladen tijdens "exec"

## EXEC
Terraform draait op je lokale pc, idk hoe je dat regelt op windows
`sudo apt install terraform`

### terraform init
om terraform bekend te maken met de plugins die gebruikt worden (en deze te downloaden) wordt "terraform init" gebruikt.

\* dit doe je in de map waar .tf files staan.

`terraform init`

### terraform apply
met terraform apply worden de "modules" aangeroepen en worden de acties uitegevoerd, je dient deze actie handmatig te bevestigen.

\* je kan -auto-approve toevoegen aan "terraform apply" (maar ik zou het niet doen)

`terraform apply`

\* Vmware maakt 1 object aan per VM, Proxmox maakt 1 object aan per VM, Netbox maakt 4 objecten aan per VM.

### terraform destroy
"terraform destroy" verwijderd alle 'ALLE' zaken die gedeployed zijn. (zelfs meer als je niet op past)
Vcenter VM's worden verwijderd.
Proxmox VM's worden verwijderd
Configuratie Netbox wordt verwijderd.

`terraform destroy`

## permanent deployment / secret management (the hacky method)
remove the following files:
- `terraform.tfstate`
- `terraform.tfstate.backup`

# SOURCES
* *niet alles is gelist
* https://github.com/cloudmaniac/terraform-deploy-vmware-vm
* https://www.virtjunkie.com/2020/04/22/vmware-provisioning-using-hashicorp-terraform/
* https://austinsnerdythings.com/2021/09/01/how-to-deploy-vms-in-proxmox-with-terraform/
* https://vectops.com/2020/05/provision-proxmox-vms-with-terraform-quick-and-easy/
* https://registry.terraform.io/providers/e-breuninger/netbox/latest/docs


# .gitignore
de .gitignore is zo ingesteld dat geen files; anders dan specifiek gespecificeerd, in git opgenomen worden.
