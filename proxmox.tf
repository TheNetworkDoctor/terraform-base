#
# MAIN section voor proxmox resource (qemu VM)
#

resource "proxmox_vm_qemu" "worker" {
  for_each = { for k, v in var.worker: k => v if v.target_node != null}
  name        = "${each.value.name}.${local.base.vm_domain}"
  desc        = each.value.name
  target_node = each.value.target_node
  os_type     = "cloud-init"
  full_clone  = true
  memory      = each.value.memory
  sockets     = "1"
  cores       = each.value.vcpu_count
  cpu         = "host"
  scsihw      = "virtio-scsi-pci"
  clone       = "${var.proxmox_vm_template}"
  agent       = 0
  bootdisk    = "scsi0"
  ciuser      = "${var.vm_username}"


  #
  # DISK section voor het maken van de disk(s)
  #

  disk {
    slot = 0
    size = "${each.value.disk_size}G"
    type = "scsi"
    storage = "${var.proxmox_datastore}"
    iothread = 1
  }

//  disk {
//    slot = 1
//    # set disk size here. leave it small for testing because expanding the disk takes time.
//    size = "${var.vm_2nd_disk_size}G"
//    type = "scsi"
//    storage = "CephPool"
//    iothread = 1
//  }

  #
  # NETWORK section voor het maken van de network(s)
  #

  network {
    model  = "virtio"
    bridge = "vmbr0"
    tag = "${var.proxmox_vm_network}"
  }

  #
  # CLOUD-INIT section voor het maken van cloud-init settings
  #

  ipconfig0 = "ip=${each.value.ip}/${each.value.netmask},gw=${each.value.gw}"
  ssh_user  = var.vm_username
  sshkeys   = var.ssh_pub_key

}