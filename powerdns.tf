# Add A record to the zone
resource "powerdns_record" "virtualmachine" {
  for_each = { for k, v in var.worker: k => v }
  zone    = "${local.base.domain_name}."
  name    = "${each.value.name}.${local.base.domain_name}."
  type    = "A"
  ttl     = 86400
  records = ["${each.value.ip}"]
}