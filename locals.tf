locals {
  base = {
    domain_name = var.domain_name
    netbox_host = "${var.netbox_host}.${var.domain_name}"
    vmware_host = "${var.vmware_host}.${var.domain_name}"
    proxmox_host = "${var.proxmox_host}.${var.domain_name}"
    powerdns_host = "${var.powerdns_host}.${var.domain_name}"
    vm_domain = "${var.vm_domain_prefix}.${var.domain_name}"
  }
  proxmox = {
    api_url = "https://${local.base.proxmox_host}:8006/api2/json"
    api_id = "${var.pm_token_id}"
    api_secret = "${var.pm_secret}"
  }
  netbox = {
    server = "https://${local.base.netbox_host}"
    api_key = var.netbox_api_key
  }
  powerdns = {
    server = "http://${local.base.powerdns_host}:8081"
    api_key = var.powerdns_api_key
  }
}
