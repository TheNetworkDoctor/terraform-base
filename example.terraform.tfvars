#####
#SECRETS
#####

#VCENTER
vsphere_user = "vcenter_user"
vsphere_password = "supersecretpassword"

#PROXMOX
pm_token_id = "proxmox_user@token_id"
pm_secret = "supersecretpassword"

#NETBOX
netbox_server = "https://netbox_url_without_path"
netbox_api_key = "supersecretpassword"

#####
#VARIABLES
#####

#ALGEMEEN
netbox_host = "netbox" // host address only, domain will be added in locals
vmware_host = "vcenter" // host address only, domain will be added in locals
proxmox_host = "proxmox.subdomain_if_present" // host address only, domain will be added in locals
domain_name = "labenv.localdomain" // domain to add to above _host addresses
vm_domain_prefix = "subdomain" // subdomain to address hosts 

#NETBOX
vsphere_cluster_id = "vcenter"
proxmox_cluster_id = "proxmox"
netbox_vm_vlan_name = "VLAN-XX"
netbox_site_id_name = "Site"

# VCENTER var config
vsphere_server = "vcenter"
vsphere_datacenter = "vcenter_datacenter"
vsphere_datastore = "vcenter_datastore" 
vsphere_compute_cluster = "vcenter_compute_cluster"
vsphere_network = "VLAN-XX"
vsphere_vm_template = "vm_template_name"
vsphere_vm_folder = "/folder/to/store/vms"
dns_server_list = ["1.1.1.1"]

# PROXMOX var config
proxmox_vm_template = "vm_template_name"
proxmox_vm_network = 4094
proxmox_datastore = "proxmox_datastore" 

#####
# WORKER VARIABLE
#####

vm_username = "username"
ssh_pub_key = "ecdsa-sha2-nistp256 AAAAEETHISISNOTAREALKEY= commentXYZ"

worker = [
  {
    name                          = "worker-1"
    ip                            = "0.0.0.0"
    gw                            = "0.0.0.0"
    netmask                       = "24"
    vcpu_count                    = "2"
    memory                        = "4096"
    disk_size                     = "50"
    target_node                   = null
    nb_role_id                    = null
    nb_tag_list                   = ["managed-by-terraform"]
  },
  {
    name                          = "worker-2"
    ip                            = "0.0.0.0"
    gw                            = "0.0.0.0"
    netmask                       = "24"
    vcpu_count                    = "2"
    memory                        = "4096"
    disk_size                     = "50"
    target_node                   = null
    nb_role_id                    = null
    nb_tag_list                   = ["managed-by-terraform"]
  }
]