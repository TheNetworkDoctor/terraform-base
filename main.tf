terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "2.1.1"
    }
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.11"
    }
    netbox = {
      source  = "e-breuninger/netbox"
      version = "~> 3.0.4"
    }
    powerdns = {
      source = "pan-net/powerdns"
      version = "1.5.0"
    }
  }
}

provider "vsphere" {
    vsphere_server = local.base.vmware_host
    user = "${var.vsphere_user}"
    password = "${var.vsphere_password}"
//    allow_unverified_ssl = true // Set as needed based on your VCenter server certificate
}

provider "proxmox" {
  pm_api_url = local.proxmox.api_url
  pm_api_token_id = var.pm_token_id
  pm_api_token_secret = var.pm_secret
//  pm_tls_insecure = true // Set as needed based on your Proxmox server certificate
}

provider "netbox" {
  server_url = local.netbox.server
  api_token  = local.netbox.api_key
//  allow_insecure_https = true // Set as needed based on your Netbox server certificate
}

# Configure the PowerDNS provider
provider "powerdns" {
  server_url = local.powerdns.server
  api_key    = local.powerdns.api_key
  insecure_https = true
}

/*
module "netbox_vm" {
    source = "./modules/netbox_vm_import"
//  depends_on = [module.netbox_vm]
}
module "vcenter_vm" {
    source = "./modules/vcenter_vm_import"
  depends_on = [module.netbox_vm]
}
module "proxmox_vm" {
    source = "./modules/proxmox_vm_import"
  depends_on = [module.netbox_vm]
}
*/


data "vsphere_datacenter" "dc" {
  name = "${var.vsphere_datacenter}"
}
data "vsphere_datastore" "datastore" {
  name          = "${var.vsphere_datastore}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_compute_cluster" "cluster" {
  name          = "${var.vsphere_compute_cluster}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_network" "network" {
  name          = "${var.vsphere_network}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_virtual_machine" "template" {
  name          = "${var.vsphere_vm_template}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}


// Assumes var vsphere_cluster_id exists as a cluster in Netbox
data "netbox_cluster" "vm_cluster_01" {
  name = "${var.vsphere_cluster_id}"
}
// Assumes var proxmox_cluster_id cluster exists as a cluster in Netbox
data "netbox_cluster" "pm_cluster_01" {
  name = "${var.proxmox_cluster_id}"
}

//data "netbox_site" "get_by_name" {
//  name = var.netbox_site_id_name
//}

# Get VLAN by name
data "netbox_vlan" "search_by_name" {
  name = var.netbox_vm_vlan_name
}

#data "netbox_device_role" "automate_role_ids" {
#  name = "automate_*"
#}

//module "server" {
//    source        = "./module_server"
//    some_variable = some_value
//}

//module "vmware_vm" {
//    source        = "./modules/vmware_vm"
//    some_variable = var.vmware_vm
//}


